package uz.pdp.apptexnobotserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppTexnoBotServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppTexnoBotServerApplication.class, args);
    }

}
